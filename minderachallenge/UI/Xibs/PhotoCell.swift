//
//  PhotoCell.swift
//  minderachallenge
//
//  Created by Tarek Jradi on 31/01/2018.
//  Copyright © 2018 Mindera. All rights reserved.
//

import UIKit
import Kingfisher

class PhotoCell : UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func bind(with photo: Photo?){
        if let p = photo {
            let resource = ImageResource(downloadURL: URL(string: "https://farm5.staticflickr.com/" +
                "\(p.server.caseEmpty)/" +
                "\(p.id.caseEmpty)_" +
                "\(p.secret.caseEmpty)_q.jpg")!, cacheKey: nil)
            imageView.kf.setImage(with: resource,
                                  placeholder: nil,
                                  options: [.transition(.fade(0.2))],
                                  progressBlock: nil, completionHandler: { (image, error, cache, url) in
                    self.titleLabel.text = p.title.caseEmpty
                })
        }
    }
}
