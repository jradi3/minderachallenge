//
//  ViewController.swift
//  minderachallenge
//
//  Created by Tarek Jradi on 30/01/2018.
//  Copyright © 2018 Mindera. All rights reserved.
//

import UIKit

class ListViewController: UICollectionViewController {

    // MARK: - Properties
    
    var objects = [Any]()
    let pb = PhotoBusiness()
    let reuseIdentifier = "PhotoCell"
    let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)

    // MARK: - IBOutlets
    @IBOutlet var errorView: UIView!
    
    // MARK: - ViewController life cicle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureViewController()
        loadList()
    }

    // MARK: - Configurations

    func configureViewController(){
        collectionView!.register(UINib.init(nibName: reuseIdentifier, bundle: nil),
                                 forCellWithReuseIdentifier: reuseIdentifier)
    }

    // MARK: - IBActions

    @IBAction func loadList(){
        self.errorView.isHidden = true
        pb.getPhotos(for: "1", success: { (result) in
            self.objects = result.rows!
            self.collectionView?.reloadData()
        }) { (error) in
            switch error {
                default: self.errorView.isHidden = false
            }
        }
    }
}

