//
//  ListViewController+DataSource.swift
//  minderachallenge
//
//  Created by Tarek Jradi on 31/01/2018.
//  Copyright © 2018 Mindera. All rights reserved.
//

import UIKit

extension ListViewController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return objects.count
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier ,for:indexPath) as! PhotoCell
        cell.bind(with: objects[indexPath.row] as? Photo)
        return cell
    }
}
