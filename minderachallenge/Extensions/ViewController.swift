//
//  ViewController.swift
//  minderachallenge
//
//  Created by Tarek Jradi on 31/01/2018.
//  Copyright © 2018 Mindera. All rights reserved.
//

import UIKit

extension UIViewController {
    func alertMessage(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
