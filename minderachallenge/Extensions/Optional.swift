//
//  Optional.swift
//  minderachallenge
//
//  Created by Tarek Jradi on 01/02/2018.
//  Copyright © 2018 Mindera. All rights reserved.
//

extension Optional {
    var caseEmpty : String {
        if self == nil {
            return ""
        }
        if "\(Wrapped.self)" == "String" {
            return "\(self!)"
        }
        return "\(self!)"
    }
}
