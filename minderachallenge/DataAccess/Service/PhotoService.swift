//
//  PhotoService.swift
//  minderachallenge
//
//  Created by Tarek Jradi on 31/01/2018.
//  Copyright © 2018 Mindera. All rights reserved.
//

import Foundation

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

public class PhotoService : Service {
    
    //
    // MARK: - .GET (Functions Data Request)
    
    /**
     get list of photos filtering with category Id
     
     - parameter page: page of photos
     - returns: Result<Photo>
     */
    
    func getPhotos(for page: String, success  : @escaping (Result<Photo>) -> (), failure  : ((_ error : Error) -> ())?) {
        PhotoService.makeGetFrom(url: "\(APIUrls.prefix)?method=flickr.photos.search&api_key=\(APIParameters.key)&tags=kitten&page=\(page)&format=json&nojsoncallback=1", success: success, failure: failure);
    }
    
    //MARK: Process Response
    
    private static func makeGetFrom<T>(url: String, success : @escaping (Result<T>) -> (), failure  : ((_ error : Error) -> ())? = nil) {
        if !PhotoService.isConnectedToInternet() { failure!(ServiceError.networkReachability) }
        Service.manager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseObject { (response: DataResponse<Result<T>>) in
            self.processCall(success: success, failure: failure, response: response)
        }
    }
    
    private static func processCall<T: BaseMappable>(success  : @escaping (T) -> (), failure  : ((_ error : Error) -> ())? = nil, response: DataResponse<T>) {
        switch (response.result) {
        case .success(let value):
            success(value)
            break;
        case .failure(let error):
            if failure != nil {
                failure!(error)
            }
            break
        }
    }
    
    
}
