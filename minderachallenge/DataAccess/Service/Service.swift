//
//  Service.swift
//  minderachallenge
//
//  Created by Tarek Jradi on 31/01/2018.
//  Copyright © 2018 Mindera. All rights reserved.
//

import Alamofire

public class Service : NSObject {
    
    //
    // MARK: - Properties
    
    static var manager: SessionManager {
        get {
            let m = SessionManager.default
            return m
        }
    }
    
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

public class APIUrls {
    static let prefix: String = "https://api.flickr.com/services/rest/"
    static var base_url_image:String = "https://img.olx.pt/images_olxpt/"
}

struct APIParameters {
    static let key = "f9cc014fa76b098f9e82f1c288379ea1"
}

enum ServiceError: Error {
    case unknownError
    case networkReachability
}
