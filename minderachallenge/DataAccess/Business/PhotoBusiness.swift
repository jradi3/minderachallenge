//
//  PhotoBusiness.swift
//  minderachallenge
//
//  Created by Tarek Jradi on 31/01/2018.
//  Copyright © 2018 Mindera. All rights reserved.
//

import Foundation
import RealmSwift

public class PhotoBusiness : NSObject {
    
    //
    // MARK: - Properties
    
    let service = PhotoService()
    
    //
    // MARK: - .GET (Functions Data Request)
    
    /**
     get list of photos filtering with category Id and write or update
     Photos at local storage.
     
     - parameter page: page of photos
     - returns: list of photos or failure.
     */
    
    func getPhotos(for page : String, success  : @escaping (Result<Photo>) -> (), failure  : ((_ error : Error) -> ())?) {
        service.getPhotos(for: page, success: { (result) in
            let results:Result<Photo> = result
            success(results)
        }, failure: { error in
            failure!(error)
        })
    }
}
