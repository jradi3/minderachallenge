//
//  Photos.swift
//  minderachallenge
//
//  Created by Tarek Jradi on 31/01/2018.
//  Copyright © 2018 Mindera. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class Result<T: Mappable>: Mappable {
    var rows: [T]?
    
    required init?(map: Map){ }
    
    func mapping(map: Map) {
        rows <- map["photos.photo"]
    }
}

class Photo: Object, Mappable {
    var id: String?
    var owner: String?
    var secret: String?
    var server: String?
    var farm: Int?
    var title: String?
    var isFriend: Int?
    var isPublic: Int?
    var isFamily: Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        owner <- map["owner"]
        secret <- map["secret"]
        server <- map["server"]
        farm <- map["farm"]
        title <- map["title"]
        isFriend <- map["isfriend"]
        isPublic <- map["ispublic"]
        isFamily <- map["isfamily"]
    }
}
