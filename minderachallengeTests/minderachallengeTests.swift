//
//  minderachallengeTests.swift
//  minderachallengeTests
//
//  Created by Tarek Jradi on 30/01/2018.
//  Copyright © 2018 Mindera. All rights reserved.
//

import XCTest
import Alamofire
@testable import minderachallenge

class minderachallengeTests: XCTestCase {
    
    var pb: PhotoBusiness!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        pb = PhotoBusiness()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        pb.getPhotos(for: "1", success: { (result) in
            XCTAssertNotNil(result)
        }) { (error) in
            XCTAssertTrue(false)
        }
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
