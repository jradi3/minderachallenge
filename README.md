# MinderaChallenge #

This application has buided with XCode 9 and Swift 4 and propose make a single screen to list a set of images fetched from a JSON API, where the images should be shown in a grid layout (2 per row).

### How do I get set up? ###

First you need clone the repository
> git clone https://jradi3@bitbucket.org/jradi3/minderachallenge.git

After that, go to the repository and intall the dependencies
> pod install

Open .xcworkspace file and run the project.

This application use XCTest for test the HTTP Request Services.